const TOKEN = "token"
const BASE_URL = "https://app.jala.tech"
const SHARE_PRICE_URL = (id: string) => {
  return BASE_URL + "/shrimp_prices/" + id
}
const SHARE_NEWS_URL = (id: string) => {
  return BASE_URL + "/posts/" + id
}
const WEBVIEW_NEWS_URL = (id: number) => {
  return BASE_URL + "/web_view/posts/" + id
}
const WEBVIEW_DISEASES_URL = (id: number) => {
  return BASE_URL + "/web_view/diseases/" + id
}

export {
  TOKEN,
  BASE_URL,
  SHARE_PRICE_URL,
  SHARE_NEWS_URL,
  WEBVIEW_NEWS_URL,
  WEBVIEW_DISEASES_URL,
}