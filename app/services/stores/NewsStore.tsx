import { createContext } from "react";
import { observable, action, makeAutoObservable, runInAction } from 'mobx';
import { ItemNewsModel } from "../../model/ItemNews";
import { UserStore } from "./UserStore";
import apiServices from '../api';
import { mapperItemNews } from "../mapper/NewsMapper";
import { MetaModel } from "../../model/meta";

export class NewsStore {
  @observable loading: boolean = true;

  @observable listNews: Array<ItemNewsModel> = [];

  @observable meta: MetaModel = {
    currentPage: 0,
    nextPage: 1,
    totalPage: 0
  };
  
  constructor() {
    makeAutoObservable(this);
  }

  @action getListNews = async (page: number) => {
    if (page <= 0) {
      return
    }
    this.loading = true
    const userStore = new UserStore();
    await userStore.getCurrentUser();
  
    const currentUser = userStore.currentUser;

    const response = await apiServices().requestGetListNews({
      token: currentUser.token,
      page: page,
    })

    if (response.ok) {
      const responseData = response.data || {};
      const responseListData = responseData.data || [];
      const responseMeta = responseData.meta || {};
      const currentPage = responseMeta.current_page || 0;
      const totalPage = responseMeta.last_page || 0;
      const nextPage = currentPage < totalPage ? currentPage + 1 : 0;

      runInAction(() => {
        const oldList = Object.assign([], this.listNews)
        oldList.push(...responseListData.map((item: any) =>
          mapperItemNews(item)
        ))
        this.listNews = oldList;

        this.meta = {
          currentPage,
          nextPage,
          totalPage,
        };
      })
    } else {

    }

    runInAction(() => {
      this.loading = false
    })
  }

  @action resetListNews = () => {
    this.meta = {
      currentPage: 0,
      nextPage: 1,
      totalPage: 0
    };
    this.listNews = [];
  }
}

const store = createContext(new NewsStore());
export default store;