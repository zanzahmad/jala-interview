import { observable, action, makeAutoObservable, runInAction } from 'mobx';
import { createContext } from 'react';
import { ItemShrimpPriceModel } from '../../model/ItemShrimpPrice';
import { MetaModel } from '../../model/meta';
import { UserStore } from './UserStore';
import apiServices from '../api';
import { mapperItemShrimpPrice } from '../mapper/ShrimpPriceMapper';
import { RegionModel } from '../../model/Region';
import { Nullable } from '../../utils/ObjectExt';

export class ListShrimpPriceStore {
  @observable loading: boolean = false

  @observable listShrimpPrice: Array<ItemShrimpPriceModel> = [];

  @observable meta: MetaModel = {
    currentPage: 0,
    nextPage: 1,
    totalPage: 0
  };
  
  constructor() {
    makeAutoObservable(this);
  }

  @action resetListShrimpPrice = () => {
    this.listShrimpPrice = [];

    this.meta = {
      currentPage: 0,
      nextPage: 1,
      totalPage: 0
    };
  }

  @action getListShrimpPrice = async (page: number, selectedRegion?: RegionModel) => {
    if (page <= 0) {
      return
    }
    this.loading = true;
    const userStore = new UserStore();
    await userStore.getCurrentUser();

    const currentUser = userStore.currentUser;

    const response = await apiServices().requestGetListShrimpPrice({
      token: currentUser.token,
      page: page,
      regionId: selectedRegion && selectedRegion.id != "" ? selectedRegion.id : undefined,
    });

    if (response.ok) {
      const responseData = response.data || {};
      const responseListData = responseData.data || [];
      const responseMeta = responseData.meta || {};
      const currentPage = responseMeta.current_page || 0;
      const totalPage = responseMeta.last_page || 0;
      const nextPage = currentPage < totalPage ? currentPage + 1 : 0;

      runInAction(() => {
        const oldList = Object.assign([], this.listShrimpPrice)
        oldList.push(...responseListData.map((item: any) =>
          mapperItemShrimpPrice(item)
        ))
        this.listShrimpPrice = oldList

        this.meta = {
          currentPage,
          nextPage,
          totalPage,
        }
      })
    } else {

    }

    runInAction(() => {
      this.loading = false;
    })
  }

}

const store = createContext(new ListShrimpPriceStore());
export default store;