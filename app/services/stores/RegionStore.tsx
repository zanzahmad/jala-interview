import { observable, action, makeAutoObservable, runInAction } from 'mobx';
import { createContext } from 'react';
import { RegionModel } from '../../model/Region';
import { MetaModel } from '../../model/meta';
import { UserStore } from './UserStore';
import apiServices from '../api';
import { mapperItemRegion } from '../mapper/ShrimpPriceMapper';
import { BottomSheetModel } from '../../model/BottomSheet';
import { mapperItemBottomSheetItem } from '../mapper/BottomSheetItemMapper';

export class RegionStore {
  @observable query: string = "";

  @observable loading: boolean = false;

  @observable listRegion: Array<RegionModel> = [];

  @observable selectedRegion: RegionModel = {
    id: '',
    name: '',
    type: '',
    latitude: '',
    longitude: '',
    countryId: '',
    countryName: '',
    countryNameUppercase: '',
    provinceId: '',
    provinceName: '',
    regencyId: '',
    regencyName: '',
    districtId: '',
    districtName: '',
    villageId: '',
    villageName: '',
    fullName: '',
    nameTranslated: '',
    countryNameTranslated: '',
    countryNameTranslatedUppercase: ''
  };

  @observable listBottomSheetRegion: Array<BottomSheetModel> = [];
  
  @observable meta: MetaModel = {
    currentPage: 0,
    nextPage: 1,
    totalPage: 0
  };

  constructor() {
    makeAutoObservable(this);
  }

  @action getListRegion = async (page: number) => {
    if (page <= 0) {
      return
    }
    this.loading = true;
    const userStore = new UserStore();
    await userStore.getCurrentUser();
  
    const currentUser = userStore.currentUser;

    const response = await apiServices().requestGetListRegion({
      token: currentUser.token,
      page: page,
      query: this.query,
    })

    if (response.ok) {
      const responseData = response.data || {};
      const responseListData = responseData.data || [];
      const responseMeta = responseData.meta || {};
      const currentPage = responseMeta.current_page || 0;
      const totalPage = responseMeta.last_page || 0;
      const nextPage = currentPage < totalPage ? currentPage + 1 : 0;

      runInAction(() => {
        const oldList = Object.assign([], this.listRegion)
        oldList.push(...responseListData.map((item: any) =>
          mapperItemRegion(item)
        ))
        this.listRegion = oldList;

        this.listBottomSheetRegion = this.listRegion.map((item: RegionModel) => 
          mapperItemBottomSheetItem(item)
        );

        this.meta = {
          currentPage,
          nextPage,
          totalPage,
        };
      })
    } else {

    }
    runInAction(() => {
      this.loading = false
    })
  }

  @action getRegionBySelectedBottomItem = async (selectedItem: BottomSheetModel) => {
    const result = this.listRegion.find((item: RegionModel) => item.id === selectedItem.id);
    this.selectedRegion = result;
  }

  timeout = null
  @action setQuery = (query: string) => {
    this.query = query;
    if (!this.loading) {
      this.loading = true
      setTimeout(() => {
        runInAction(() => {
          this.listBottomSheetRegion = [];
          this.listRegion = [];
          this.meta = {
            currentPage: 0,
            nextPage: 1,
            totalPage: 0,
          };
          this.getListRegion(this.meta.nextPage);
        })
      }, 2000);
    }
  }
}

const store = createContext(new RegionStore());
export default store;