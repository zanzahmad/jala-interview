import { createContext } from "react";
import { observable, action, makeAutoObservable, runInAction } from 'mobx';
import { UserStore } from "./UserStore";
import apiServices from '../api';
import { MetaModel } from "../../model/meta";
import { ItemDiseasesModel } from "../../model/ItemDiseases";
import { mapperItemDiseases } from "../mapper/DiseasesMapper";

export class DiseasesStore {
  @observable loading: boolean = true;

  @observable listDiseases: Array<ItemDiseasesModel> = [];

  @observable meta: MetaModel = {
    currentPage: 0,
    nextPage: 1,
    totalPage: 0
  };
  
  constructor() {
    makeAutoObservable(this);
  }

  @action getListDiseases = async (page: number) => {
    if (page <= 0) {
      return
    }
    this.loading = true
    const userStore = new UserStore();
    await userStore.getCurrentUser();
  
    const currentUser = userStore.currentUser;

    const response = await apiServices().requestGetListDiseases({
      token: currentUser.token,
      page: page,
    })

    if (response.ok) {
      const responseData = response.data || {};
      const responseListData = responseData.data || [];
      const responseMeta = responseData.meta || {};
      const currentPage = responseMeta.current_page || 0;
      const totalPage = responseMeta.last_page || 0;
      const nextPage = currentPage < totalPage ? currentPage + 1 : 0;

      runInAction(() => {
        const oldList = Object.assign([], this.listDiseases)
        oldList.push(...responseListData.map((item: any) =>
        mapperItemDiseases(item)
        ))
        this.listDiseases = oldList;

        this.meta = {
          currentPage,
          nextPage,
          totalPage,
        };
      })
    } else {

    }

    runInAction(() => {
      this.loading = false
    })
  }

  @action resetListDiseases = () => {
    this.meta = {
      currentPage: 0,
      nextPage: 1,
      totalPage: 0
    };
    this.listDiseases = [];
  }
}

const store = createContext(new DiseasesStore());
export default store;