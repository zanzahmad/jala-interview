import { observable, action, makeAutoObservable, runInAction } from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TOKEN } from '../constants/constants';
import { createContext } from 'react';

export type User = {
  token: string;
}

export class UserStore {
  @observable loading: boolean = true

  @observable currentUser: User = {
    token: ""
  };

  constructor() {
    makeAutoObservable(this);
  }

  @action setCurrentUserToken = async (token: string) => {    
    try {
      await AsyncStorage.setItem(TOKEN, token);
    } catch (error) {
      console.log(error);
    }
    runInAction(() => {
      this.loading = false;
    })
  }

  @action getCurrentUser = async () => {
    const tokenUser = await AsyncStorage.getItem(TOKEN);
    runInAction(() => {
      this.currentUser = {
        token: tokenUser || "" 
      };
    })
  }

  @action
  loadingState() {
    return this.loading
  }
}

const store = createContext(new UserStore());
export default store;