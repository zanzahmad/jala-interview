import { BottomSheetModel } from "../../model/BottomSheet"
import { RegionModel } from "../../model/Region"
import { capitalizeEachWord } from "../../utils/StringExt"

const mapperItemBottomSheetItem = (itemRegion: RegionModel) => {
  const result: BottomSheetModel = {
    id: itemRegion.id,
    text: capitalizeEachWord(itemRegion.fullName)
  } 
  return result
}

export {
  mapperItemBottomSheetItem,
}