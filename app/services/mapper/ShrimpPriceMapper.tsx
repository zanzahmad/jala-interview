import { CreatorModel } from "../../model/Creator"
import { ItemShrimpPriceModel } from "../../model/ItemShrimpPrice"
import { PriceModel } from "../../model/Price"
import { RegionModel } from "../../model/Region"
import { getShrimpSizeText } from "../../utils/StringExt"

const mapperItemRegion = (dataRegion: any) => {
  const region: RegionModel = {
    id: dataRegion.id || "",
    name: dataRegion.name || "",
    type: dataRegion.type || "",
    latitude: dataRegion.latitude || "",
    longitude: dataRegion.longitude || "",
    countryId: dataRegion.country_id || "",
    countryName: dataRegion.country_name || "",
    countryNameUppercase: dataRegion.country_name_uppercase || "",
    provinceId: dataRegion.province_id || "",
    provinceName: dataRegion.province_name || "",
    regencyId: dataRegion.regency_id || "",
    regencyName: dataRegion.regency_name || "",
    districtId: dataRegion.district_id || "",
    districtName: dataRegion.district_name || "",
    villageId: dataRegion.village_id || "",
    villageName: dataRegion.village_name || "",
    fullName: dataRegion.full_name || "",
    nameTranslated: dataRegion.name_translated || "",
    countryNameTranslated: dataRegion.country_name_translated || "",
    countryNameTranslatedUppercase: dataRegion.country_name_translated_uppercase || "",
  }

  return region;
}

const mapperItemShrimpPrice = (data : any) => {
  const dataRegion = data.region || {}
  const region: RegionModel = mapperItemRegion(dataRegion)

  const dataCreator = data.creator || {}
  const dataCreatorSettings = dataCreator.settings || {}
  const dataCreatorState = dataCreator.state || {}
  const creator: CreatorModel = {
    id: dataCreator.id || 0,
    roleId: dataCreator.role_id || 0,
    name: dataCreator.name || "",
    email: dataCreator.email || "",
    avatar: dataCreator.avatar || "",
    emailVerified: dataCreator.email_verified || "",
    subscriptionTypeId: dataCreator.subscription_type_id || 0,
    settings: {
      locale: dataCreatorSettings.locale || ""
    },
    createdAt: dataCreator.created_at || "",
    updatedAt: dataCreator.updated_at || "",
    regionId: dataCreator.region_id || "",
    address: dataCreator.address || "",
    lastLoginAt: dataCreator.last_login_at || "",
    deactivated: dataCreator.deactivated || "",
    expiredAt: dataCreator.expired_at || "",
    phone: dataCreator.phone || "",
    phoneVerified: dataCreator.phone_verified || "",
    gender: dataCreator.gender || "",
    occupation: dataCreator.occupation || "",
    idNumber: dataCreator.id_number || "",
    idScan: dataCreator.id_scan || "",
    tinNumber: dataCreator.tin_number || "",
    tinScan: dataCreator.tin_scan || "",
    birthdate: dataCreator.birthdate || "",
    company: dataCreator.company || "",
    companyAddress: dataCreator.company_address || "",
    position: dataCreator.position || "",
    monthlyIncome: dataCreator.monthly_income || "",
    incomeSource: dataCreator.income_source || "",
    buyer: dataCreator.buyer || false,
    phoneCountry: dataCreator.phone_country || "",
    country: dataCreator.country || "",
    interest: dataCreator.interest || "",
    unsubscribeEmailAt: dataCreator.unsubscribe_email_at || "",
    freshchatRestoreId: dataCreator.freshchat_restore_id || "",
    allowCreateClient: dataCreator.allow_create_client || "",
    allowCreateToken: dataCreator.allow_create_token || "",
    interests: dataCreator.interests || [],
    state: {
      phoneVerificationExpiredAt: dataCreatorState.phone_verification_expired_at || "",
      homeCoachmarkShown: dataCreatorState.home_coachmark_shown || "",
      consultationShown: dataCreatorState.consultation_shown || "",
    },
    familyCardNumber: dataCreator.family_card_number || "",
    familyCardScan: dataCreator.family_card_scan || "",
    telegramId: dataCreator.telegram_id || "",
    genderName: dataCreator.gender_name || "",
    expiredDate: dataCreator.expired_date || "",
    expiredTime: dataCreator.expired_time || "",
    upcomingBirthdate: dataCreator.upcoming_birthdate || "",
  }

  const result: ItemShrimpPriceModel = {
    id: data.id || 0,
    speciesId: data.species_id || 0,
    date: data.date || "",
    size20: data.size_20 || 0,
    size30: data.size_30 || 0,
    size40: data.size_40 || 0,
    size50: data.size_50 || 0,
    size60: data.size_60 || 0,
    size70: data.size_70 || 0,
    size80: data.size_80 || 0,
    size90: data.size_90 || 0,
    size100: data.size_100 || 0,
    size110: data.size_110 || 0,
    size120: data.size_120 || 0,
    size130: data.size_130 || 0,
    size140: data.size_140 || 0,
    size150: data.size_150 || 0,
    size160: data.size_160 || 0,
    size170: data.size_170 || 0,
    size180: data.size_180 || 0,
    size190: data.size_190 || 0,
    size200: data.size_200 || 0,
    remark: data.remark || "",
    createdBy: data.created_by || 0,
    updatedBy: data.updated_by || 0,
    createdAt: data.created_at || "",
    updatedAt: data.updated_at || "",
    regionId: data.region_id || "",
    contact: data.contact || "",
    countryId: data.country_id || "",
    currencyId: data.currency_id || "",
    week: data.week || 0,
    dateRegionFullName: data.date_region_full_name || "",
    provinceId: data.province_id || "",
    regencyId: data.regency_id || "",
    districtId: data.district_id || "",
    villageId: data.village_id || "",
    region: region,
    creator: creator
  }

  return result 
}

const sizes = [
  "size20",
  "size30",
  "size40",
  "size50",
  "size60",
  "size70",
  "size80",
  "size90",
  "size100",
  "size110",
  "size120",
  "size130",
  "size140",
  "size150",
  "size160",
  "size170",
  "size180",
  "size190",
  "size200",
]

const mapperListPrice = (item: ItemShrimpPriceModel) => {
  const result: Array<PriceModel> = []
  Object.keys(item).forEach((key: string) => {
    if (sizes.includes(key) && item[key] > 0) {
      result.push({
        key,
        keyText: getShrimpSizeText(key),
        value: item[key]
      })
    }
  });

  return result
}

export {
  mapperItemShrimpPrice,
  mapperItemRegion,
  mapperListPrice,
}