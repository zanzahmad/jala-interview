import { ItemNewsModel } from "../../model/ItemNews";

const mapperItemNews = (dataNews: any) => {
  const result: ItemNewsModel = {
    id: dataNews.id || 0,
    authorId: dataNews.author_id || 0,
    categoryId: dataNews.category_id || 0,
    image: dataNews.image || "",
    status: dataNews.status || "",
    featured: dataNews.featured || false,
    advertisement: dataNews.advertisement || "",
    createdAt: dataNews.created_at || "",
    updatedAt: dataNews.updated_at || "",
    title: dataNews.title || "",
    seoTitle: dataNews.seo_title || "",
    excerpt: dataNews.excerpt || "",
    body: dataNews.body || "",
    slug: dataNews.slug || "",
    metaDescription: dataNews.meta_description || "",
    metaKeywords: dataNews.meta_keywords || "",
  }
  return result;
}

export {
  mapperItemNews,
}