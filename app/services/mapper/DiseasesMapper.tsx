import { ItemDiseasesModel } from "../../model/ItemDiseases";

const mapperItemDiseases = (dataDiseases: any) => {
  const result: ItemDiseasesModel = {
    id: dataDiseases.id || 0,
    fullName: dataDiseases.full_name || "",
    shortName: dataDiseases.short_name || "",
    image: dataDiseases.image || "",
    slug: dataDiseases.slug || "",
    metaDescription: dataDiseases.meta_description || "",
    metaKeywords: dataDiseases.meta_keywords || "",
    status: dataDiseases.status || "",
    indication: dataDiseases.indication || "",
    pathogen: dataDiseases.pathogen || "",
    effect: dataDiseases.effect || "",
    stabilityViability: dataDiseases.stabilityViability || "",
    handling: dataDiseases.handling || "",
    regulation: dataDiseases.regulation || "",
    reference: dataDiseases.reference || "",
    createdAt: dataDiseases.created_at || "",
    updatedAt: dataDiseases.updated_at || "",
  }
  return result;
}

export {
  mapperItemDiseases,
}