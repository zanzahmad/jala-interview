import apisauce from 'apisauce';
import { BASE_URL } from '../constants/constants'

// define the api
const create = () => {
  const api = apisauce.create({
    baseURL: BASE_URL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 10000
  })

  const header = (token: string) => {
    return {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
  }

  const requestGetListShrimpPrice = async (
    {
      token,
      page,
      regionId = undefined,
      perPage = 15,
      withValue = "region,creator",
    }: {
      token: string
      perPage?: number
      page: number
      withValue?: string
      regionId?: string | undefined
    }
  ) => {
    const data = await api.get(
      `api/shrimp_prices`,
      {
        "per_page": perPage,
        "page": page,
        "with": withValue,
        "region_id": regionId,
      },
      header(token)
    )

    return data
  }

  const requestGetListRegion = async (
    {
      token,
      page,
      query = undefined,
      perPage = 25,
      hasValue = "shrimp_prices",
    }: {
      token: string
      perPage?: number
      page: number
      hasValue?: string
      query?: string | undefined
    }
  ) => {
    const data = await api.get(
      `api/regions`,
      {
        "per_page": perPage,
        "page": page,
        "has": hasValue,
        "search": query,
      },
      header(token)
    )

    return data
  }

  const requestGetListNews = async (
    {
      token,
      page,
      perPage = 25,
      withValue = "creator",
    }: {
      token: string
      page: number
      perPage?: number
      withValue?: string
    }
  ) => {
    const data = await api.get(
      `api/posts`,
      {
        "per_page": perPage,
        "page": page,
        "with": withValue,
      },
      header(token)
    )

    return data
  }

  const requestGetListDiseases = async (
    {
      token,
      page,
      perPage = 25,
    }: {
      token: string
      page: number
      perPage?: number
    }
  ) => {
    const data = await api.get(
      `api/diseases`,
      {
        "per_page": perPage,
        "page": page,
      },
      header(token)
    )

    return data
  }

  return {
    requestGetListShrimpPrice,
    requestGetListRegion,
    requestGetListNews,
    requestGetListDiseases,
  }
};

export default create