import { BottomSheetModel } from "../model/BottomSheet"

const ListSize: Array<BottomSheetModel> = [
  {
    id: "size20",
    text: "20",
  },
  {
    id: "size30",
    text: "30",
  },
  {
    id: "size40",
    text: "40",
  },
  {
    id: "size50",
    text: "50",
  },
  {
    id: "size60",
    text: "60",
  },
  {
    id: "size70",
    text: "70",
  },
  {
    id: "size80",
    text: "80",
  },
  {
    id: "size90",
    text: "90",
  },
  {
    id: "size100",
    text: "100",
  },
  {
    id: "size110",
    text: "110",
  },
  {
    id: "size120",
    text: "120",
  },
  {
    id: "size130",
    text: "130",
  },
  {
    id: "size140",
    text: "140",
  },
  {
    id: "size150",
    text: "150",
  },
  {
    id: "size160",
    text: "160",
  },
  {
    id: "size170",
    text: "170",
  },
  {
    id: "size180",
    text: "180",
  },
  {
    id: "size190",
    text: "190",
  },
  {
    id: "size200",
    text: "200",
  },
]

export default ListSize