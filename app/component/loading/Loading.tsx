import * as React from 'react';
import {
  View,
  Text,
} from 'react-native';

// Styles
import styles from './LoadingStyle'

function Loading({
  loading,
}: LoadingProps) {
  if (!loading) {
    return null
  }
  return (
    <View style={styles.container}>
      <Text style={styles.loadingText}>Loading...</Text>
    </View>
  );
}

type LoadingProps = React.PropsWithChildren<{
  loading: Boolean
}>;


export default Loading;