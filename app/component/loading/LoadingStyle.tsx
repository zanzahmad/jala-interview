import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
		flex: 1,
		justifyContent: 'center',
		alignContent: 'center',
		marginTop: 14
	},
  loadingText: {
		alignSelf: 'center',
		fontFamily: Fonts.latoRegular,
		fontSize: 14,
		lineHeight: 24,
	},
})