import * as React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import Images from '../../images';

// Styles
import styles from './HeaderBackStyle'

function HeaderBack(props: HeaderBackProps) {
  return (
    <TouchableWithoutFeedback onPress={props.onPressButton}>
      <View style={styles.containerBackButton}>
        <Image source={Images.arrowBack} />
      </View>  
    </TouchableWithoutFeedback>
  );
}

type HeaderBackProps = React.PropsWithChildren<{
  onPressButton: () => void;
}>;


export default HeaderBack;