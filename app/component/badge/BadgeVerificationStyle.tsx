import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
    height: 22,
    flexDirection: 'row',
    backgroundColor: Colors.yellowLight,
    paddingHorizontal: 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 27,
  },
  containerUnverified: {
    height: 22,
    flexDirection: 'row',
    backgroundColor: Colors.gray,
    paddingHorizontal: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 27,
  },
  textIsVerified: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.textColor,
    marginStart: 3
  },
  textIsUnverified: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.textColor,
    marginStart: 0
  }
})