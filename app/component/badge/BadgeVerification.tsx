import * as React from 'react';
import {
  View,
  Text,
  Image
} from 'react-native';
import Images from '../../images';

// Styles
import styles from './BadgeVerificationStyle'

function BadgeVerification({
  icon = Images.verificationBadge,
  isVerified
}: BadgeVerificationProps) {
  const verificationText = isVerified ? "Terverifikasi" : "belum terverifikasi"
  return (
    <View style={isVerified ? styles.container : styles.containerUnverified}>
      { isVerified &&
        <Image
          source={icon} />
      }
      <Text style={isVerified ? styles.textIsVerified : styles.textIsUnverified}>{verificationText}</Text>
    </View>
  );
}

type BadgeVerificationProps = React.PropsWithChildren<{
  icon: any
  isVerified: Boolean
}>;


export default BadgeVerification;