import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    width: 24,
    height: 24,
  },
  icon: {
    width: 24,
    height: 24,
  }
})