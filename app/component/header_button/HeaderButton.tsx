import * as React from 'react';
import {
  View,
  Image,
  TouchableWithoutFeedback
} from 'react-native';
import Images from '../../images';

// Styles
import styles from './HeaderButtonStyle'

function HeaderButton({
  icon = Images.share,
  onPressHeaderButton,
}: HeaderButtonProps) {
  return (
    <TouchableWithoutFeedback
      onPress={onPressHeaderButton}>
      <View style={styles.container}>
        <Image
          style={styles.icon}
          source={icon} />
      </View>
    </TouchableWithoutFeedback>
  );
}

type HeaderButtonProps = React.PropsWithChildren<{
  icon?: any,
  onPressHeaderButton: () => void;
}>;


export default HeaderButton;