import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  containerButtonDefault: {
    paddingHorizontal: 18,
    paddingVertical: 6,
    borderRadius: 4,
    backgroundColor: Colors.blue,
    alignSelf: 'center'
  },
  text: {
    lineHeight: 20,
    fontSize: 14,
    fontFamily: Fonts.latoBold,
    color: Colors.white
  }
})