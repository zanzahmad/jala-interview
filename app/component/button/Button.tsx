import * as React from 'react';
import {
  View,
  Text,
  StyleProp,
  ViewStyle,
  GestureResponderEvent,
  TouchableOpacity
} from 'react-native';

// Styles
import styles from './ButtonStyle'

function Button({
  buttonText,
  buttonStyles,
  onPressButton,
}: ButtonProps) {
  return (
    <TouchableOpacity style={{ ...styles.containerButtonDefault, ...(buttonStyles as {}) }} onPress={onPressButton}>
      <View>
        <Text style={styles.text}>{buttonText}</Text>
      </View>
    </TouchableOpacity>
  );
}

type ButtonProps = React.PropsWithChildren<{
  buttonText: string,
  buttonStyles: StyleProp<ViewStyle>;
  onPressButton: (event: GestureResponderEvent) => void
}>;


export default Button;