import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: Colors.gray,
    marginTop: 8,
    marginHorizontal: 18,
    padding: 12,
    backgroundColor: Colors.white,
  },
  containerProfile: {
    flexDirection: 'row',
    flex: 1,
  },
  containerProfileName: {
    flexDirection: 'column',
    flex: 1,
    marginStart: 8,
    paddingBottom: 8
  },
  profilePhoto: {
    width: 32,
    height: 32,
    borderRadius: 18,
  },
  profileRole: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.blueGray,
  },
  profileName: {
    fontFamily: Fonts.latoRegular,
    fontSize: 14,
    color: Colors.textColor,
  },
  dateShrimpPriceText: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    marginTop: 10,
    color: Colors.blueGray,
  },
  provinceText: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    marginTop: 4,
    color: Colors.textColor,
  },
  regencyText: {
    fontFamily: Fonts.latoBold,
    fontSize: 18,
    color: Colors.textColor,
    lineHeight: 24,
  },
  containerBottom: {
    flex: 1,
    marginTop: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  containerPrice: {
    flex: 1,
    flexDirection: 'column',
  },
  sizeText: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.blueGray,
  },
  priceText: {
    fontFamily: Fonts.latoBlack,
    fontSize: 22,
    color: Colors.textColor,
  },
  buttonShowDetail: {
    alignSelf: 'center',
  }
})
