import * as React from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  TouchableWithoutFeedback,
  Image
} from 'react-native';
import ListSize from '../../constant/ListSize';
import { BottomSheetModel } from '../../model/BottomSheet';
import { ItemShrimpPriceModel } from '../../model/ItemShrimpPrice';
import { getPricebySizeItemShrimpPriceModel } from '../../utils/ItemShrimpPriceExt';
import { formattedDateMonthYear } from '../../utils/DateExt';
import { getFormattedPriceText } from '../../utils/NumberExt';
import { capitalizeEachWord, formattedImageUrl, getShrimpSizeText } from '../../utils/StringExt';
import BadgeVerification from '../badge/BadgeVerification';
import Button from '../button/Button';

// Styles
import styles from './ItemShrimpPriceStyle'

function ItemShrimpPrice({
  selectedSize,
	data,
  onPressItem,
}: ItemShrimpPriceProps) {
  const selectedShrimpSize = selectedSize?.id || ""
  const price = getPricebySizeItemShrimpPriceModel(data, selectedShrimpSize)

  return (
    <TouchableWithoutFeedback
      onPress={onPressItem}
      style={{}}>
      <View style={styles.container}>
        <View style={styles.containerProfile}>
          <Image
            style={styles.profilePhoto}
            source={{
              uri: formattedImageUrl(data.creator.avatar)
            }} />
          <View style={styles.containerProfileName}>
            <Text style={styles.profileRole}>Supplier</Text>
            <Text style={styles.profileName}>{data.creator.name}</Text>
          </View>
          <BadgeVerification isVerified={data.creator.buyer} />
        </View>
        <Text style={styles.dateShrimpPriceText}>{formattedDateMonthYear(data.date)}</Text>
        <Text style={styles.provinceText}>{capitalizeEachWord(data.region.provinceName)}</Text>
        <Text style={styles.regencyText}>{capitalizeEachWord(data.region.regencyName)}</Text>
        <View style ={styles.containerBottom}>
          <View style ={styles.containerPrice}>
            <Text style={styles.sizeText}>{getShrimpSizeText(selectedShrimpSize)}</Text>
            <Text style={styles.priceText}>{getFormattedPriceText(price)}</Text>
          </View>
          <Button
            onPressButton={onPressItem}
            buttonStyles={styles.buttonShowDetail}
            buttonText='LIHAT DETAIL' />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

type ItemShrimpPriceProps = React.PropsWithChildren<{
  selectedSize: BottomSheetModel | null;
	data: ItemShrimpPriceModel;
  onPressItem: (event: GestureResponderEvent) => void;
}>;

export default ItemShrimpPrice;