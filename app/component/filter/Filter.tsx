import * as React from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  TouchableWithoutFeedback,
  Image
} from 'react-native';
import Images from '../../images';
import { BottomSheetModel } from '../../model/BottomSheet';
import { RegionModel } from '../../model/Region';
import { capitalizeEachWord } from '../../utils/StringExt';

// Styles
import styles from './FilterStyle'

function Filter({
  selectedRegion,
  selectedSize,
  onPressSelectSize,
  onPressSelectRegion,
}: BadgeVerificationProps) {  
  const region = selectedRegion.id ? capitalizeEachWord(selectedRegion.nameTranslated) : "Indonesia"
  return (
    <View style={styles.containerFilter}>
      <TouchableWithoutFeedback onPress={onPressSelectSize}>
        <View style={styles.containerSize}>
          <Image source={Images.biomass} />
          <View style={styles.containerSizeItem}>
            <Text style={styles.hintSizeText}>Size</Text>
            <Text style={styles.sizeText}>{selectedSize.text}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={onPressSelectRegion}>
        <View style={styles.containerRegion}>
          <Image source={Images.location} />
          <Text style={styles.regionText}>{region}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

type BadgeVerificationProps = React.PropsWithChildren<{
  selectedSize: BottomSheetModel;
  selectedRegion: RegionModel;
  onPressSelectSize: (event: GestureResponderEvent) => void;
  onPressSelectRegion: (event: GestureResponderEvent) => void;
}>;


export default Filter;