import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  containerFilter: {
    position: 'absolute',
    borderRadius: 60,
    bottom: 8,
    start: 12,
    end: 12,
    flexDirection: 'row',
    overflow: 'hidden',
  },
  containerSize: {
    flex: 1.3,
    flexDirection: 'row',
    paddingStart: 18,
    backgroundColor: Colors.darkBlue,
    alignItems: 'center',
  },
  containerSizeItem: {
    paddingVertical: 8,
    marginStart: 8,
  },
  hintSizeText: {
    fontSize: 12,
    fontFamily: Fonts.latoRegular,
    color: Colors.white,
  },
  sizeText: {
    fontSize: 14,
    fontFamily: Fonts.latoBold,
    color: Colors.white,
  },
  containerRegion: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: Colors.blue,
    alignItems: 'center',
    paddingHorizontal: 18,
  },
  regionText: {
    fontSize: 16,
    fontFamily: Fonts.latoBold,
    color: Colors.white,
    marginStart: 8,
  },
})