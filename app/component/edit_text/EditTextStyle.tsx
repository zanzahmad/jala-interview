import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    alignContent: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.gray,
    backgroundColor: Colors.lightBlueGray,
  },
  search: {
    alignSelf: 'center'
  },
  input: {
    flex: 1,
    fontFamily: Fonts.latoRegular,
    fontSize: 16,
    color: Colors.textColor,
    marginStart: 4,
  }
})