import * as React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleProp,
  ViewStyle,
} from 'react-native';
import Images from '../../images';
import { Colors } from '../../theme';

// Styles
import styles from './EditTextStyle'

function EditText({
  inputValue,
  inputStyle,
  onChangeText,
}: EditTextProps) {
  return (
    <View style={{...styles.container, ...(inputStyle as {})}}>
      <Image
        style={styles.search}
        source={Images.search} />
      <TextInput
        style={styles.input}
        value={inputValue}
        onChangeText={text => {
          onChangeText(text)
        }}
        placeholderTextColor={Colors.darkGray}
        placeholder={"Cari"} />
    </View>
  );
}

type EditTextProps = React.PropsWithChildren<{
  inputValue: string,
  inputStyle: StyleProp<ViewStyle>;
  onChangeText: (text: string) => void;
}>;


export default EditText;