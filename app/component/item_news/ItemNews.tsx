import * as React from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback
} from 'react-native';
import Images from '../../images';
import { ItemDiseasesModel } from '../../model/ItemDiseases';
import { ItemNewsModel } from '../../model/ItemNews';
import { formattedFullDateMonthYear } from '../../utils/DateExt';
import { formattedImageUrl } from '../../utils/StringExt';

// Styles
import styles from './ItemNewsStyle'

function ItemNews({
  item,
  onPressItemNews,
  onPressShareNews,
}: ItemNewsProps) {
  const itemNews = item as ItemNewsModel
  const itemDiseases = item as ItemDiseasesModel
  return (
    <TouchableWithoutFeedback onPress={onPressItemNews}>
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={{
            uri: formattedImageUrl(item.image)
          }} />
        <Text style={styles.title}>{itemNews.title || itemDiseases.fullName}</Text>
        <Text
          numberOfLines={2}
          ellipsizeMode={'tail'}
          style={styles.description}>{item.metaDescription}</Text>
        <View style={styles.containerBottom}>
          <Text style={styles.date}>{formattedFullDateMonthYear(item.createdAt)}</Text>
          <TouchableWithoutFeedback onPress={onPressShareNews}>
            <Image style={styles.share} source={Images.share} />
          </TouchableWithoutFeedback>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

type ItemNewsProps = React.PropsWithChildren<{
  item: ItemNewsModel | ItemDiseasesModel;
  onPressItemNews: () => void;
  onPressShareNews: () => void;
}>;


export default ItemNews;