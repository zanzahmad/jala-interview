import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginTop: 12,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: Colors.gray,
    backgroundColor: Colors.white,
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: 160,
  },
  title: {
    marginHorizontal: 12,
    marginTop: 8,
    fontFamily: Fonts.latoBlack,
    fontSize: 18,
    lineHeight: 24,
    color: Colors.textColor,
  },
  description: {
    marginHorizontal: 12,
    marginTop: 4,
    fontFamily: Fonts.latoRegular,
    fontSize: 14,
    lineHeight: 20,
    color: Colors.lightTextColor,
  },
  containerBottom: {
    marginHorizontal: 12,
    marginVertical: 8,
    flexDirection: 'row',
  },
  date: {
    flex: 1,
    fontFamily: Fonts.latoRegular,
    fontSize: 14,
    lineHeight: 20,
    color: Colors.lightTextColor,
  },
  share: {
    tintColor: Colors.textColor
  },
})