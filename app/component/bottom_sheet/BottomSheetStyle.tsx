import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {  
  },
  header: {
    flexDirection: 'row',
    marginTop: 16,
    paddingHorizontal: 16,
  },
  containerSubheader: {
    flexDirection: 'row',
    paddingTop: 8,
    borderBottomColor: Colors.gray,
  },
  listItem: {
    marginTop: 8,
    borderTopWidth: 2,
    borderColor: Colors.gray,
  },
  title: {
    flex: 1,
    fontFamily: Fonts.latoBold,
    fontSize: 16,
    color: Colors.textColor,
  },
  close: {
    fontFamily: Fonts.latoBold,
    fontSize: 14,
    color: Colors.darkBlue,
  },
  containerItem: {
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  textItem: {
    fontFamily: Fonts.latoRegular,
    color: Colors.textColor,
    fontSize: 14,
  },
  input: {
    flex: 1,
    borderRadius: 4,
    marginStart: 16,
  },
  clearIcon: {
    alignSelf: 'center',
    marginStart: 12,
    marginEnd: 20,
  }
})