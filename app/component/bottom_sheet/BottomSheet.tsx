import * as React from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
  Image
} from 'react-native';
import Images from '../../images';
import { BottomSheetModel } from '../../model/BottomSheet';
import EditText from '../edit_text/EditText';
import EmptyState from '../empty_state/EmptyState';
import Loading from '../loading/Loading';

// Styles
import styles from './BottomSheetStyle'

function BottomSheet({
  title,
  listData,
  querySearch = '',
  isSearchEnabled = false,
  loading = false,
  onSearchQuery,
  onEndReachedList,
  onPressSelectData,
}: BottomSheetProps) {
  const renderEmpty = () => (
    <EmptyState loading={loading} />
  );

  const renderFooter = () => (
    <Loading loading={loading} />
  );

  const renderItem = React.useCallback(({item} : {item: BottomSheetModel}) => (
    <TouchableWithoutFeedback
      onPress={() => onPressSelectData(item)}
      key={item.id.toString()}>
      <View style={styles.containerItem}>
        <Text style={styles.textItem}>{item.text}</Text>
      </View>
    </TouchableWithoutFeedback>
  ), []);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
        <TouchableWithoutFeedback
          onPress={() => onPressSelectData({id: "", text: ""})}>
          <Text style={styles.close}>Tutup</Text>          
        </TouchableWithoutFeedback>
      </View>
      { isSearchEnabled &&
        <View style={styles.containerSubheader}>
          <EditText
            inputValue={querySearch}
            inputStyle={styles.input}
            onChangeText={(text) => {
              onSearchQuery(text)
            }} />
          <TouchableWithoutFeedback
            onPress={() => {
              onSearchQuery("")
            }}>
            <Image
              style={styles.clearIcon}
              source={Images.danger} />
          </TouchableWithoutFeedback>
        </View>
      }
      <FlatList
        style={styles.listItem}
        ListFooterComponent={renderFooter()}
        ListEmptyComponent={renderEmpty()}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 100}}
        onEndReached={onEndReachedList}
        data={listData}
        renderItem={renderItem} />
    </View>
  );
}

type BottomSheetProps = React.PropsWithChildren<{
  title: string;
  listData: Array<BottomSheetModel>;
  querySearch: string;
  isSearchEnabled: boolean;
  loading: boolean;
  onSearchQuery: (text: string) => void;
  onEndReachedList: () => void;
  onPressSelectData: (item: BottomSheetModel) => void;
}>;


export default BottomSheet;