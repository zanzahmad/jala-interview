import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingTop: 10
  },
  dateContainer: {
    flexDirection: 'row'
  },
  date: {
    fontFamily: Fonts.latoRegular,
    fontSize: 14,
    color: Colors.lightTextColor,
    flex: 1,
  },
  containerProfile: {
    flexDirection: 'row',
    marginTop: 8,
  },
  containerProfileName: {
    flexDirection: 'column',
    flex: 1,
    marginStart: 8,
    paddingBottom: 8
  },
  profilePhoto: {
    width: 32,
    height: 32,
    borderRadius: 18,
  },
  profileRole: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.darkGray,
  },
  profileName: {
    fontFamily: Fonts.latoBold,
    fontSize: 14,
    color: Colors.textColor,
    lineHeight: 20
  },
  containerContact: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  containerPhone: {
    flex: 1,
    flexDirection: 'column',
  },
  hintPhone: {
    fontFamily: Fonts.latoRegular,
    fontSize: 12,
    color: Colors.darkGray,
    lineHeight: 16,
  },
  phoneText: {
    fontFamily: Fonts.latoBold,
    fontSize: 16,
    color: Colors.textColor,
    lineHeight: 24,
  },
  button: {
    alignSelf: 'center'
  }
})