import * as React from 'react';
import {
  View,
  Image,
  TouchableWithoutFeedback,
  Text
} from 'react-native';
import Images from '../../images';
import { ItemShrimpPriceModel } from '../../model/ItemShrimpPrice';
import { formattedDateMonthYear } from '../../utils/DateExt';
import { formattedImageUrl, formattedPhoneString } from '../../utils/StringExt';
import BadgeVerification from '../badge/BadgeVerification';
import Button from '../button/Button';

// Styles
import styles from './SupplierSectionStyle'

function SupplierSection({
  item,
  onPressCall,
}: SupplierSectionProps) {
  return (
    <View style={styles.container}>
      <View style= {styles.dateContainer}>
        <Text style={styles.date}>{formattedDateMonthYear(item.date)}</Text>
        <BadgeVerification icon={Images.verified} isVerified={item.creator.buyer} />
      </View>
      <View style={styles.containerProfile}>
        <Image
          style={styles.profilePhoto}
          source={{
            uri: formattedImageUrl(item.creator.avatar)
          }} />
        <View style={styles.containerProfileName}>
          <Text style={styles.profileRole}>Supplier</Text>
          <Text style={styles.profileName}>{item.creator.name}</Text>
        </View>
      </View>
      <View style={styles.containerContact}>
        <View style={styles.containerPhone}>
          <Text style={styles.hintPhone}>Kontak</Text>
          <Text style={styles.phoneText}>{formattedPhoneString(item.creator.phone)}</Text>
        </View>
        <Button
          onPressButton={onPressCall}
          buttonStyles={styles.button}
          buttonText='Hubungi' />
      </View>
    </View>
  );
}

type SupplierSectionProps = React.PropsWithChildren<{
  item: ItemShrimpPriceModel;
  onPressCall: () => void; 
}>;


export default SupplierSection;