import * as React from 'react';
import {
  View,
  Text,
} from 'react-native';

// Styles
import styles from './EmptyStateStyle'

function EmptyState({
  loading,
}: EmptyStateProps) {
  if (loading) {
    return null
  }
  return (
    <View style={styles.container}>
      <Text style={styles.loadingText}>List Item Kosong...</Text>
    </View>
  );
}

type EmptyStateProps = React.PropsWithChildren<{
  loading: Boolean
}>;


export default EmptyState;