const Images = {
  location: require('./location.png'),
  biomass: require('./biomass.png'),
  verificationBadge: require('./verification-badge.png'),
  search: require('./search.png'),
  danger: require('./danger.png'),
  share: require('./share.png'),
  verified: require('./verified.png'),
  arrowBack: require('./arrow-back.png'),
};

export default Images