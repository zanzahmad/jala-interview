import * as React from 'react';
import { View, Text, FlatList, Share, RefreshControl } from 'react-native';
import { observer } from 'mobx-react';
import ItemNews from '../../component/item_news/ItemNews';
import { ItemDiseasesModel } from '../../model/ItemDiseases';
import { shareDiseasesMessage } from '../../utils/StringExt';
import DiseasesStore from '../../services/stores/DiseasesStore';

// Styles
import styles from './ListDiseasesStyle';
import EmptyState from '../../component/empty_state/EmptyState';
import Loading from '../../component/loading/Loading';
import { Colors } from '../../theme';

const ListDiseasesScreen = observer(({navigation} : {navigation: any}) => {
  const diseasesStore = React.useContext(DiseasesStore);
  const [didMount, setDidMount] = React.useState(true);

  React.useEffect(() => {
    if (didMount) {
      diseasesStore.getListDiseases(diseasesStore.meta.nextPage)
      setDidMount(false)
    }
  }, [didMount]);

  const renderHeader = React.useCallback(() => (
    <View style={styles.containerHeader}>
      <Text style={styles.headerText}>Daftar Penyakit</Text>
    </View>
  ), []);

  const renderEmpty = () => (
    <EmptyState loading={diseasesStore.loading} />
  );

  const renderFooter = () => (
    <Loading loading={diseasesStore.loading} />
  );

  const renderItem = React.useCallback(({item} : {item: ItemDiseasesModel}) => (
    <ItemNews
      item={item}
      onPressItemNews={() => {
        navigation.navigate('DetailNewsScreen', { item })
      }}
      onPressShareNews={async () => {
        await Share.share({
          message: shareDiseasesMessage(item),
        })
      }}
      key={item.id.toString()} />
  ), []);

  const loadMoreListDiseases = () => {
    if (!diseasesStore.loading) {
      diseasesStore.getListDiseases(diseasesStore.meta.nextPage)
    }
  }

  const onRefresh = () => {
    diseasesStore.resetListDiseases()
    diseasesStore.getListDiseases(diseasesStore.meta.nextPage)
  }

  return (
    <View style={styles.mainContainer}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={[Colors.darkBlue]}
            refreshing={diseasesStore.loading && diseasesStore.meta.nextPage === 1}
            onRefresh={onRefresh}
          />
        }
        ListHeaderComponent={renderHeader}
        ListFooterComponent={renderFooter()}
        ListEmptyComponent={renderEmpty()}
        contentContainerStyle={styles.contentContainer}
        data={diseasesStore.listDiseases}
        renderItem={renderItem}
        onEndReached={() => loadMoreListDiseases()} />
    </View>
  );
})

export default ListDiseasesScreen;