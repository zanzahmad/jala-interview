import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { StatusBar, View } from 'react-native';
import { Colors, Fonts } from '../../theme';
import ListDiseasesScreen from '../list_diseases/ListDiseases';
import ListNewsScreen from '../list_news/ListNews';
import ListShrimpPriceScreen from '../list_shrimp_price/ListShrimpPrice';

const Tab = createMaterialTopTabNavigator();

function HomeScreen() {
  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={Colors.primary} />
      <Tab.Navigator
        screenOptions={{
          tabBarItemStyle: {
            height: 52
          },
          tabBarLabelStyle: {
            fontSize: 14,
            fontFamily: Fonts.latoBold,
            textTransform: 'none'
          },
          tabBarIndicatorStyle: {
            backgroundColor: Colors.primary,
            height: 2
          },
        }} >
        <Tab.Screen name="Harga Udang" component={ListShrimpPriceScreen} />
        <Tab.Screen name="Kabar Udang" component={ListNewsScreen} />
        <Tab.Screen name="Penyakit Udang" component={ListDiseasesScreen} />
      </Tab.Navigator>
    </View>
  );
}

export default HomeScreen;