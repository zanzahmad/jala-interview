import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  ...ApplicationStyles,
  contentContainer: {
    backgroundColor: Colors.white,
    paddingBottom: 20,
  },
  containerHeader: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: 14
  },
  headerText: {
    alignSelf: 'flex-start',
    color: Colors.darkBlue,
    fontFamily: Fonts.latoBold,
    fontSize: 18,
    marginHorizontal: 20,
    lineHeight: 24,
  },
})
