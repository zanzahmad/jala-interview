import * as React from 'react';
import { View, Text, FlatList, Share, RefreshControl } from 'react-native';
import ItemNews from '../../component/item_news/ItemNews';
import { ItemNewsModel } from '../../model/ItemNews';
import { observer } from 'mobx-react';
import { shareNewsMessage } from '../../utils/StringExt';
import NewsStore from '../../services/stores/NewsStore';

// Styles
import styles from './ListNewsStyle';
import Loading from '../../component/loading/Loading';
import EmptyState from '../../component/empty_state/EmptyState';
import { Colors } from '../../theme';

const  ListNewsScreen = observer(({ navigation }: { navigation: any }) => {
  const newsStore = React.useContext(NewsStore);
  const [didMount, setDidMount] = React.useState(true);

  React.useEffect(() => {
    if (didMount) {
      newsStore.getListNews(newsStore.meta.nextPage)
      setDidMount(false)
    }
  }, [didMount]);

  const renderHeader = React.useCallback(() => (
    <View style={styles.containerHeader}>
      <Text style={styles.headerText}>Kabar terbaru</Text>
    </View>
  ), []);

  const renderEmpty = () => (
    <EmptyState loading={newsStore.loading} />
  );

  const renderFooter = () => (
    <Loading loading={newsStore.loading} />
  );

  const renderItem = React.useCallback(({item} : {item: ItemNewsModel}) => (
    <ItemNews
      item={item}
      onPressItemNews={() => {
        navigation.navigate('DetailNewsScreen', { item })
      }}
      onPressShareNews={async () => {
        await Share.share({
          message: shareNewsMessage(item),
        })
      }}
      key={item.id.toString()} />
  ), []);

  const loadMoreListNews = () => {
    if (!newsStore.loading) {
      newsStore.getListNews(newsStore.meta.nextPage)
    }
  }

  const onRefresh = () => {
    newsStore.resetListNews()
    newsStore.getListNews(newsStore.meta.nextPage)
  }

  return (
    <View style={styles.mainContainer}>
      <FlatList
        refreshControl={
          <RefreshControl
            colors={[Colors.darkBlue]}
            refreshing={newsStore.loading && newsStore.meta.nextPage === 1}
            onRefresh={onRefresh}
          />
        }
        ListHeaderComponent={renderHeader}
        ListFooterComponent={renderFooter()}
        ListEmptyComponent={renderEmpty()}
        contentContainerStyle={styles.contentContainer}
        data={newsStore.listNews}
        renderItem={renderItem}
        onEndReached={() => loadMoreListNews()} />
    </View>
  );
})

export default ListNewsScreen;