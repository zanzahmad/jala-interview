import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../theme'

export default StyleSheet.create({
  ...ApplicationStyles,
  containerWebview: {
    margin: 12,
    borderRadius: 8,
    overflow: 'hidden',
  },
})
