import * as React from 'react';
import { View, Share, ScrollView } from 'react-native';
import HeaderButton from '../../component/header_button/HeaderButton';
import { ItemNewsModel } from '../../model/ItemNews';
import { shareDiseasesMessage, shareNewsMessage } from '../../utils/StringExt';
import WebView from 'react-native-webview';


// Styles
import styles from './DetailNewsStyle'
import { WEBVIEW_DISEASES_URL, WEBVIEW_NEWS_URL } from '../../services/constants/constants';
import { ItemDiseasesModel } from '../../model/ItemDiseases';
import Loading from '../../component/loading/Loading';

function DetailNewsScreen({ route, navigation } : { route: any, navigation: any }) {
  const itemNews = route.params.item as ItemNewsModel;
  const itemDiseases = route.params.item as ItemDiseasesModel;
  const title: string = itemNews.title ? "Kabar Udang" : "Info Penyakit";
  const [webviewHeight, setWebviewHeight] = React.useState(0)
  const [loading, setLoading] = React.useState(true)

  React.useEffect(() => {
    navigation.setOptions({
      title: title,
      headerRight: () => (
        <HeaderButton
          onPressHeaderButton={async () => {
            await Share.share({
              message: itemNews.title ? shareNewsMessage(itemNews) : shareDiseasesMessage(itemDiseases)
            })
          }} />
      ),
    });
  }, [navigation]);
  
  const onProductDetailsWebViewMessage = (event: any) => {
    setWebviewHeight(Number(event.nativeEvent.data))
  }

  const uri = itemNews.title ? WEBVIEW_NEWS_URL(itemNews.id) : WEBVIEW_DISEASES_URL(itemDiseases.id)
  return (
    <ScrollView>
      <View
        renderToHardwareTextureAndroid={true}
        style={styles.containerWebview}>
        <WebView
          style={{height: webviewHeight + 30}}
          originWhitelist={['*']}
          onLoad={() => setLoading(false)}
          onMessage={onProductDetailsWebViewMessage}
          startInLoadingState={false}
          injectedJavaScript='window.ReactNativeWebView.postMessage(document.body.scrollHeight)'
          source={{uri}} />
        <Loading loading={loading} />
      </View>
    </ScrollView>
  );
}

export default DetailNewsScreen;