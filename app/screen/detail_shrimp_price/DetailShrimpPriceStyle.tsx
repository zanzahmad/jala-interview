import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../theme'

export default StyleSheet.create({
  ...ApplicationStyles,
  containerLocationSection: {
    backgroundColor: Colors.white,
    padding: 12,
  },
  provinceName: {
    fontFamily: Fonts.latoBold,
    color: Colors.textColor,
    fontSize: 16,
    lineHeight: 24,
  },
  regencyName: {
    fontFamily: Fonts.latoBold,
    color: Colors.lightTextColor,
    fontSize: 16,
    lineHeight: 24,
  },
  containerDetail: {
    marginTop: 8,
    backgroundColor: Colors.white,
    marginBottom: 24,
  },
  listPhone: {
    fontFamily: Fonts.latoBold,
    fontSize: 16,
    lineHeight: 24,
    color: Colors.textColor,
    marginHorizontal: 16,
    marginTop: 16,
  },
  containerListPrice: {
    marginHorizontal: 16,
    marginTop: 12,
    flexDirection: 'row',
  },
  priceHint: {
    fontFamily: Fonts.latoRegular,
    color: Colors.textColor,
    fontSize: 16,
    lineHeight: 20,
    width: 80,
  },
  price: {
    fontFamily: Fonts.latoRegular,
    color: Colors.textColor,
    fontSize: 16,
    lineHeight: 20,
  },
  noteTitle: {
    fontFamily: Fonts.latoBold,
    fontSize: 16,
    color: Colors.textColor,
    lineHeight: 24,
    marginHorizontal: 16,
    marginTop: 18,
  },
  noteText: {
    fontFamily: Fonts.latoRegular,
    fontSize: 14,
    color: Colors.textColor,
    lineHeight: 20,
    marginHorizontal: 16,
    marginTop: 4,
    marginBottom: 8,
  },
})
