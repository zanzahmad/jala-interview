import * as React from 'react';
import { View, Text, Share, Linking, FlatList, ScrollView } from 'react-native';
import HeaderButton from '../../component/header_button/HeaderButton';
import SupplierSection from '../../component/supplier_section/SupplierSection';
import { ItemShrimpPriceModel } from '../../model/ItemShrimpPrice';
import { PriceModel } from '../../model/Price';
import { SHARE_PRICE_URL } from '../../services/constants/constants';
import { mapperListPrice } from '../../services/mapper/ShrimpPriceMapper';
import { getFormattedPriceTextID } from '../../utils/NumberExt';
import { capitalizeEachWord } from '../../utils/StringExt';

// Styles
import styles from './DetailShrimpPriceStyle'

function DetailShrimpPriceScreen({ route, navigation }: { route: any, navigation: any}) {
  const item: ItemShrimpPriceModel = route.params.item;

  const shareMessage = () => {
    return "Jala Media - Harga udang " + capitalizeEachWord(item.region.fullName) + "\nlink: " + SHARE_PRICE_URL(item.id.toString())
  }

  React.useEffect(() => {
    navigation.setOptions({
      title: 'Harga Udang',
      headerRight: () => (
        <HeaderButton
          onPressHeaderButton={async () => {
            await Share.share({
              message: shareMessage()
            })
          }} />
      ),
    });
  }, [navigation]);

  const renderItem = React.useCallback(({item} : {item: PriceModel}) => (
    <View key={item.key}>
      <Text>{item.keyText}</Text>
      <Text>{item.value}</Text>
    </View>
      
  ), []);

  const listPrice: Array<PriceModel> = mapperListPrice(item)

  return (
    <ScrollView>
      <View style={styles.mainContainer}>
        <View style={styles.containerLocationSection}>
          <Text style={styles.provinceName}>{capitalizeEachWord(item.region.provinceName)}</Text>
          { item.region.regencyName &&
            <Text style={styles.regencyName}>
              {capitalizeEachWord(item.region.regencyName)}
              {item.region.districtName && `, ${capitalizeEachWord(item.region.districtName)}`}
            </Text>
          }
        </View>
        <View style={styles.containerDetail}>
          <SupplierSection
            onPressCall={() => {
              Linking.openURL(`tel:${item.creator.phone}`)
            }}
            item={item} />
          <Text style={styles.listPhone}>Daftar Harga</Text>
          { listPrice.map((item: PriceModel) => {
           return (
            <View style={styles.containerListPrice} key={item.key}>
              <Text style={styles.priceHint}>{item.keyText}</Text>
              <Text style={styles.price}>{getFormattedPriceTextID(item.value)}</Text>
            </View>
          )})}
          <Text style={styles.noteTitle}>Catatan</Text>
          <Text style={styles.noteText}>{item.remark || "-"}</Text>
        </View>
      </View>
    </ScrollView>
    
  );
}

export default DetailShrimpPriceScreen;