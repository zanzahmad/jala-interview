import * as React from 'react';
import { View, Text, FlatList, GestureResponderEvent, RefreshControl } from 'react-native';
import Filter from '../../component/filter/Filter';
import ItemShrimpPrice from '../../component/item_shrimp_price/ItemShrimpPrice';
import { ItemShrimpPriceModel } from '../../model/ItemShrimpPrice';
import RBSheet from "react-native-raw-bottom-sheet";
import { observer } from 'mobx-react';
import BottomSheet from '../../component/bottom_sheet/BottomSheet';
import { BottomSheetModel } from '../../model/BottomSheet';
import ListSize from '../../constant/ListSize';
import { windowHeight } from '../../constant/Dimension';

import ShrimpPriceStore from '../../services/stores/ShrimpPriceStore';
import RegionStore from '../../services/stores/RegionStore';

// Styles
import styles from './ListShrimpPriceStyle'
import Loading from '../../component/loading/Loading';
import EmptyState from '../../component/empty_state/EmptyState';
import { Colors } from '../../theme';

const ListShrimpPriceScreen = observer(({navigation} : {navigation: any}) => {
  const shrimpPriceStore = React.useContext(ShrimpPriceStore);
  const regionStore = React.useContext(RegionStore);
  const [didMount, setDidMount] = React.useState(true);

  React.useEffect(() => {
    if (didMount) {
      shrimpPriceStore.getListShrimpPrice(shrimpPriceStore.meta.nextPage)
      regionStore.getListRegion(shrimpPriceStore.meta.nextPage)
      setDidMount(false)
    }
  }, [didMount]);

  const [openSizeWindow, setOpenSizeWindow] = React.useState(false);
  const [openRegionWindow, setOpenRegionWindow] = React.useState(false);

  const [selectedSize, setSelectedSize] = React.useState(ListSize[0])

  const [query, setQuery] = React.useState("");

  const renderItem = React.useCallback(({item} : {item: ItemShrimpPriceModel}) => (
    <ItemShrimpPrice
      selectedSize={selectedSize}
      key={item.id.toString()}
      data={item}
      onPressItem={() => {
        navigation.navigate('DetailShrimpPriceScreen', {
          item: item,
        })
      }} />
  ), [selectedSize]);

  const renderHeader = React.useCallback(() => (
    <View style={styles.containerHeader}>
      <Text style={styles.headerText}>Harga terbaru</Text>
    </View>
  ), []);

  const renderEmpty = () => (
    <EmptyState loading={shrimpPriceStore.loading} />
  );

  const renderFooter = () => (
    <Loading loading={shrimpPriceStore.loading} />
  );

  const loadMoreListPrice = () => {
    if (!shrimpPriceStore.loading) {
      shrimpPriceStore.getListShrimpPrice(shrimpPriceStore.meta.nextPage, regionStore.selectedRegion)
    }
  }

  const loadMoreListBottomModel = () => {
    if (openRegionWindow && !regionStore.loading) {
      regionStore.getListRegion(regionStore.meta.nextPage)
    }
  }

  let refRBSheet: React.RefObject<any>  = React.useRef();

  const onSelectData = (item: BottomSheetModel) => {
    if (openSizeWindow && item.id !== "") {
      setSelectedSize(item);
    } else if (openRegionWindow && item.id !== "") {
      regionStore.getRegionBySelectedBottomItem(item)
      shrimpPriceStore.resetListShrimpPrice()
      shrimpPriceStore.getListShrimpPrice(shrimpPriceStore.meta.nextPage, regionStore.selectedRegion)
    }
    refRBSheet.current ? refRBSheet.current.close() : {};
  }

  const onRefresh = () => {
    shrimpPriceStore.resetListShrimpPrice()
    shrimpPriceStore.getListShrimpPrice(shrimpPriceStore.meta.nextPage, regionStore.selectedRegion)
  }

  const listModel = openSizeWindow ? ListSize : openRegionWindow ? regionStore.listBottomSheetRegion : [];
  const title = openSizeWindow ? "Size" : openRegionWindow ? "Kota/kabupaten" : "";

  return (
    <View style={styles.mainContainer}>
      <FlatList
        contentContainerStyle={styles.contentContainer}
        refreshControl={
          <RefreshControl
            colors={[Colors.darkBlue]}
            refreshing={shrimpPriceStore.loading && shrimpPriceStore.meta.nextPage === 1}
            onRefresh={onRefresh}
          />
        }
        ListHeaderComponent={renderHeader}
        ListFooterComponent={renderFooter()}
        ListEmptyComponent={renderEmpty()}
        renderItem={renderItem}
        onEndReached={() => loadMoreListPrice()}
        data={shrimpPriceStore.listShrimpPrice} />
      <Filter
        selectedSize={selectedSize}
        selectedRegion={regionStore.selectedRegion}
        onPressSelectSize={() => {
          refRBSheet.current.open()
          setOpenSizeWindow(true)
        }}
        onPressSelectRegion={() => {
          refRBSheet.current.open()
          setOpenRegionWindow(true)
        }} />
      <RBSheet
        ref={refRBSheet}
        height={windowHeight - 50}
        closeOnPressMask={true}
        onClose={() => {
          setOpenSizeWindow(false)
          setOpenRegionWindow(false)
        }}
        customStyles={{
          container: {
            borderTopEndRadius: 16,
            borderTopStartRadius: 16,
          }
        }} >
        <BottomSheet
          title={title}
          listData={listModel}
          loading={openRegionWindow && regionStore.loading}
          onPressSelectData={onSelectData}
          onEndReachedList={loadMoreListBottomModel}
          querySearch={query}
          onSearchQuery={(text) => {
            setQuery(text)
            regionStore.setQuery(text)
          }}
          isSearchEnabled={openRegionWindow} />
      </RBSheet>
    </View>
  );
})

export default ListShrimpPriceScreen;