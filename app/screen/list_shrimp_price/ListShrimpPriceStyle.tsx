import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../theme'

export default StyleSheet.create({
	...ApplicationStyles,
	contentContainer: {
		backgroundColor: Colors.white,
		paddingBottom: 100
	},
	containerHeader: {
		flex: 1,
		justifyContent: 'center',
		alignContent: 'center',
		marginTop: 14
	},
	headerText: {
		alignSelf: 'center',
		color: Colors.darkBlue,
		fontFamily: Fonts.latoBold,
		fontSize: 18,
		lineHeight: 24,
	},
	emptyText: {
		alignSelf: 'center',
		fontFamily: Fonts.latoRegular,
		fontSize: 14,
		lineHeight: 24,
	},
})
