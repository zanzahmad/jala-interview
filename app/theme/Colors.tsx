const Colors = {
  primary: "#1B77DF",
  white: "#FFF",
  gray: "#E5E5E5",
  darkGray: "#A09E9E",
  blueGray: "#859ED1",
  lightBlueGray: "#F5F6F7",
  LightBlue: "#F1F5F9",
  textColor: "#454646",
  lightTextColor: "#737373",
  yellowLight: "#FFF8E7",
  blue: "#1B77DF",
  darkBlue: "#004492",
}

export default Colors
