import { StyleSheet } from 'react-native'
import Colors from './Colors'

const ApplicationStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.LightBlue
  },
})

export default ApplicationStyles
