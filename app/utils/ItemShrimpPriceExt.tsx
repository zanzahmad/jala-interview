import { ItemShrimpPriceModel } from "../model/ItemShrimpPrice"

const getPricebySizeItemShrimpPriceModel = (
  data: ItemShrimpPriceModel,
  size: string
) => {
  return data[size] || 0
}

export {
  getPricebySizeItemShrimpPriceModel,
}