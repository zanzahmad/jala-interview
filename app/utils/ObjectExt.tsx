const objectToValuesPolyfill = (object: any) => {
  return Object.keys(object).map(key => object[key]);
};

export type Nullable<T> = {
  [P in keyof T as null extends T[P] ? P : never]: T[P]
}

export {
  objectToValuesPolyfill,
}