const formattedDateMonthYear = (date: String) => {
  const splittedDate = date.split("-").map(value => 
    Number(value)
  )

  return splittedDate[2] + " " + monthString[splittedDate[1] + 1] + " " + splittedDate[0]
}

const formattedFullDateMonthYear = (date: string) => {
  const dateObj = new Date(date)

  return dateObj.getDate() + " " + monthString[dateObj.getMonth()] + " " + dateObj.getFullYear()
}

const monthString = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
]

export {
  formattedDateMonthYear,
  formattedFullDateMonthYear,
}