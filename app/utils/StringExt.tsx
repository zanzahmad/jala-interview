import { ItemDiseasesModel } from "../model/ItemDiseases";
import { ItemNewsModel } from "../model/ItemNews";
import { SHARE_NEWS_URL } from "../services/constants/constants";

const formattedImageUrl = (avatar: string) => {
  return "https://app.jala.tech/storage/" + avatar 
}

const formattedPhoneString = (phone: string) => {
  return phone.replace(/\d{3}$/, "XXX");
}

const capitalizeEachWord = (string: string) => {
  if (!string) {
    return ""
  }
  var splitStr = string.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
  }
  return splitStr.join(' '); 
}

const getShrimpSizeText = (selectedShrimpSize: string) => {
  const sizes:  Record<string, string> = {
    "size20": "size 20",
    "size30": "size 30",
    "size40": "size 40",
    "size50": "size 50",
    "size60": "size 60",
    "size70": "size 70",
    "size80": "size 80",
    "size90": "size 90",
    "size100": "size 100",
    "size110": "size 110",
    "size120": "size 120",
    "size130": "size 130",
    "size140": "size 140",
    "size150": "size 150",
    "size160": "size 160",
    "size170": "size 170",
    "size180": "size 180",
    "size190": "size 190",
    "size200": "size 200",
  }

  return sizes[selectedShrimpSize]
}

const shareNewsMessage = (item: ItemNewsModel) => {
  return item.title + "\nlink: " + SHARE_NEWS_URL(item.id.toString())
}

const shareDiseasesMessage = (item: ItemDiseasesModel) => {
  return "Jala Media - " + item.fullName + "\nlink: " + SHARE_NEWS_URL(item.id.toString())
}

export {
  formattedImageUrl,
  formattedPhoneString,
  capitalizeEachWord,
  getShrimpSizeText,
  shareNewsMessage,
  shareDiseasesMessage,
}