const getFormattedPriceText = (price: number) => {
  if (price === 0) {
    return "Kosong"
  }
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "IDR"
  }).format(price).replace(".00", "").replace(",",".")
}

const getFormattedPriceTextID = (price: number) => {
  if (price === 0) {
    return "Kosong"
  }
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR"
  }).format(price).replace(",00", "")
}

export {
  getFormattedPriceText,
  getFormattedPriceTextID,
}