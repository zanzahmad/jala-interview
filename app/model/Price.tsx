export type PriceModel = {
  key: string,
  keyText: string,
  value: number,
};