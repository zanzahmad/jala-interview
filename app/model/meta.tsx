export type MetaModel = {
	currentPage: number;
  nextPage: number;
  totalPage: number;
};
