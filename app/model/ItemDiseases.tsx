export type ItemDiseasesModel = {
	id: number;
	fullName: number;
	shortName: number;
	image: string;
	slug: string;
  metaDescription: string;
	metaKeywords: string;
	status: string;
  indication: string;
  pathogen: string;
  effect: string;
  stabilityViability: string;
  handling: string;
  regulation: string;
  reference: string;
	createdAt: string;
	updatedAt: string;
};
