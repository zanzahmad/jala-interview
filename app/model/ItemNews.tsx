export type ItemNewsModel = {
	id: number;
	authorId: number;
	categoryId: number;
	image: string;
	status: string;
	featured: boolean;
	advertisement: string;
	createdAt: string;
	updatedAt: string;
	title: string;
	seoTitle: string;
	excerpt: string;
	body: string;
	slug: string;
	metaDescription: string;
	metaKeywords: string;
};
