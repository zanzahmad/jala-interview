type SettingsModel = {
	locale: string;
}

type StateModel = {
	phoneVerificationExpiredAt: string;
	homeCoachmarkShown: string;
	consultationShown: string;
}

export type CreatorModel = {
	id: number;
	roleId: number;
	name: string;
	email: string;
	avatar: string;
	emailVerified: string;
	subscriptionTypeId: number;
	settings: SettingsModel;
	createdAt: string;
	updatedAt: string;
	regionId: string;
	address: string;
	lastLoginAt: string;
	deactivated: string;
	expiredAt: string;
	phone: string;
	phoneVerified: Boolean;
	gender: string;
	occupation: string;
	idNumber: string;
	idScan: string;
	tinNumber: string;
	tinScan: string;
	birthdate: string;
	company: string;
	companyAddress: string;
	position: string;
	monthlyIncome: string;
	incomeSource: string;
	buyer: boolean;
	phoneCountry: string;
	country: string;
	interest: string;
	unsubscribeEmailAt: string;
	freshchatRestoreId: string;
	allowCreateClient: string;
	allowCreateToken: string;
	interests: Array<string>;
	state: StateModel;
	familyCardNumber: string;
	familyCardScan: string;
	telegramId: string;
	genderName: string;
	expiredDate: string;
	expiredTime: string;
	upcomingBirthdate: string;
};
