# Jala Media



## Requirement Document

- [ ] [Document](https://paper.dropbox.com/doc/Software-Engineer-Mobile-App-React-Native-Test-JALA-Okma5EiNVRsAl325o5yYQ)

## Dependencies

- [ ] Core
```
react-native
react 18.2.0
```
- [ ] State Management
```
mobx
mobx-react
```
- [ ] Navigation
```
@react-navigation
```
- [ ] Networking
```
apisauce
```

## Demo App

- [ ] [Record](https://drive.google.com/file/d/1wNEsloY_NgejXsyk_sPEowBQAkcY0JfU/view?usp=sharing)
